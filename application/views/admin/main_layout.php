<?php $this->load->view('admin/templates/head'); ?>
<body>
    <nav class="navbar navbar-default navbar-static-top ">
        <div class="container">
            <a class="navbar-brand" href="<?php echo site_url('admin/dashboard'); ?>"><img src="<?php echo site_url('/public_html/img/CMS_logo_small.png'); ?>"></img></a>
            <ul class="nav navbar-nav" style="padding: 25px 0px;">
                <li class="active"><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
                <li><?php echo anchor('admin/page', 'pages'); ?></li>
		<li><?php echo anchor('admin/page/order', 'order pages'); ?></li>
		<li><?php echo anchor('admin/article', 'news articles'); ?></li>
		<li><?php echo anchor('admin/user', 'users'); ?></li>
            </ul>
            <ul class="dropdown dropdown-menu-right nav navbar-nav navbar-right" style="padding: 25px 0;">
                <button class="btn btn-default dropdown-toggle dropdown-menu-right glyphicon glyphicon-cog" style="height: 45px;margin-top: 5px;margin-bottom: 5px;
width: 65px;" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                  <li role="presentation"><?php echo anchor('admin/user/profile', '<i class="glyphicon glyphicon-user"></i> User Profile'); ?></li>
                 <li role="presentation"><?php echo anchor('admin/user/settings', '<i class="glyphicon glyphicon-wrench"></i> Settings'); ?></li>
                  <li role="presentation"><?php echo anchor('admin/user/logout', '<i class="glyphicon glyphicon-off"></i> logout'); ?></li>
                </ul>
            </ul>
        </div>
    </nav>
<?php $this->load->view('admin/templates/tail'); ?>