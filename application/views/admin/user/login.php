<div class="header">
    <h3>Login in</h3>
    <p>Please use your credentials</p>
</div>
<div class="modal-body">
    <?php echo validation_errors(); ?>
    <?php echo form_open('admin/dashboard',array('class' => 'form-signin', 'id' => 'myform')); ?>
      <div class="form-group" style="width: 180px;margin: 5px auto;text-align: left;">
        <label for="username"><div class="glyphicon glyphicon-user"></div> User Name</label>
        <?php echo form_input(array('name' => 'username','class' => 'form-control')); ?>
      </div>
      <div class="form-group" style="width: 180px;margin: 5px auto;text-align: left;">
        <label for="password"><div class="glyphicon glyphicon-lock"></div> Password</label>
          <?php echo form_password(array('name' => 'password','class' => 'form-control')); ?>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox"> Remember me
        </label>
      </div>
      <?php echo form_submit('submit', 'Log in', 'class="btn btn-primary"'); ?>
    <?php echo form_close(); ?>
</div>
<div class="modal-header"></div>