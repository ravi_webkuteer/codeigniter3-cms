<?php
class Migration_Create_settings extends CI_Migration {
    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'Setting Name' => array(
                'type' => 'VARCHAR',
                'constraint' => '128',
            ),
            'Setting Value' => array(
                'type' => 'VARCHAR',
                'constraint' => '128',
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('ci_settings');
    }
    public function down()
    {
	$this->dbforge->drop_table('ci_settings');
    }
}
