<?php
Class Dashboard extends Backend_Controller{
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        $this->load->view('admin/main_layout', $this->data);
    }
    public function modal() {
        $this->load->view('admin/modal_layout', $this->data);
    }
}


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

